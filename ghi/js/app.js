function createCard(name, description, pictureUrl, startDate, endDate, location) {
  return `
    <div class="col mb-4">
      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">Start Date: ${startDate}</small>
          <br>
          <small class="text-muted">End Date: ${endDate}</small>
        </div>
      </div>
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error('An error has occurred.');
    } else {
      const data = await response.json();

      const cardContainer = document.getElementById('cardContainer');

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const location = details.conference.location.name;
          const name = details.conference.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts).toLocaleDateString();
          const endDate = new Date(details.conference.ends).toLocaleDateString();
          const html = createCard(name, description, pictureUrl, startDate, endDate, location);
          cardContainer.insertAdjacentHTML('beforeend', html);
        }
      }
    }
  } catch (e) {
    console.error('Error:', e.message);
  }
});
